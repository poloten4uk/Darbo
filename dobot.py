import serial
import time
import subprocess

class Dobot(object):
    # dobot object
    
    ser = ''
    status = ''

    def __init__(self):
        # find serial port where arduino is
        dev = subprocess.check_output('ls /dev/ttyACM*', shell=True)
        try:
            self.ser = serial.Serial(dev.strip(), 9600)
            print 'Arduino connected'
            self.status = 'Dobot ready to go!'
        except:
            print 'Arduino false'
            self.status = 'Dobot not ready!'


    def sendRequest(self, ldirection, lspeed, rdirection, rspeed):
        self.ser.write('{},{},{},{}'.format(ldirection, lspeed, rdirection, rspeed))
        print('{},{},{},{}'.format(ldirection, lspeed, rdirection, rspeed))
        
    def getStatus(self):
        return self.status
