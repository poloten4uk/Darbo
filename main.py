from flask import *
from dobot import Dobot

app = Flask(__name__)
dobot = Dobot()

@app.route("/")
def init():
    ldirection = int(request.args.get('ldirection'))
    lspeed = int(request.args.get('lspeed'))
    rdirection = int(request.args.get('rdirection'))
    rspeed = int(request.args.get('rspeed'))
    
    dobot.sendRequest(ldirection, lspeed, rdirection, rspeed)
    
    return ('', 204)

@app.route("/status")
def status():
    return dobot.getStatus()

if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True)
