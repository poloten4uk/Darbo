import RPi.GPIO as io # always needed with RPi.GPIO  
from time import sleep  # pull in the sleep function from time module  
  
io.setmode(io.BCM)  # choose BCM or BOARD numbering schemes. I use BCM  
  
io.setup(23, io.OUT)# set GPIO 23 as output for white led  
io.setup(24, io.OUT)# set GPIO 24 as output for red led  
io.setup(25, io.OUT)# set GPIO 25 as output for red led  
  
speed = io.PWM(23, 100)    # create object white for PWM on port 25 at 100 Hertz  
  
speed.start(0)              # start white led on 0 percent duty cycle (off)  
  
# now the fun starts, we'll vary the duty cycle to   
# dim/brighten the leds, so one is bright while the other is dim  
  
pause_time = 0.02           # you can change this to slow down/speed up  
  
try:  
    while True:  
        for i in range(0,101):      # 101 because it stops when it finishes 100  
            speed.ChangeDutyCycle(i)  
            sleep(pause_time)  
        for i in range(100,-1,-1):      # from 100 to zero in steps of -1  
            speed.ChangeDutyCycle(i) 
            sleep(pause_time)  
  
except KeyboardInterrupt:  
    speed.stop()            # stop the white PWM output  
    io.cleanup()          # clean up GPIO on CTRL+C exit  